"""
Created on Tue Sep  8 15:20:33 2020
@author: sandra khavi huynh truong
<sandra.huynhtruong@corteva.com>
"""

import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import pandas as pd
import numpy as np
import pymc3 as pm
import arviz as az
import theano
import theano.tensor as tt
from numpy.ma import masked_values
import pickle
import sys
import time

from c4_photosynthesis_model import fAj, fCm, fAc1, fA, fTemp
from references import DI_REF_INPUTS

def buildModel():
    t_theta_P25 = tt.scalar('t_theta_P25',  dtype = 'float64'); t_theta_P25.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["theta"]["P25"]
    t_theta_Topt = tt.scalar('t_theta_Topt',  dtype = 'float64'); t_theta_Topt.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["theta"]["Topt"]
    t_theta_beta = tt.scalar('t_theta_beta',  dtype = 'float64'); t_theta_beta.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["theta"]["beta"]

    t_alpha_PSII_P25 = tt.scalar('t_alpha_PSII_P25',  dtype = 'float64'); t_alpha_PSII_P25.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["alpha_PSII"]["P25"]
    t_alpha_PSII_Topt = tt.scalar('t_alpha_PSII_Topt',  dtype = 'float64'); t_alpha_PSII_Topt.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["alpha_PSII"]["Topt"]
    t_alpha_PSII_beta = tt.scalar('t_alpha_PSII_beta',  dtype = 'float64'); t_alpha_PSII_beta.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["alpha_PSII"]["beta"]
    
    t_JmaxT_P25 = tt.scalar('t_JmaxT_P25',  dtype = 'float64'); t_JmaxT_P25.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["JmaxT"]["P25"]
    t_JmaxT_Topt = tt.scalar('t_JmaxT_Topt',  dtype = 'float64'); t_JmaxT_Topt.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["JmaxT"]["Topt"]
    t_JmaxT_beta = tt.scalar('t_JmaxT_beta',  dtype = 'float64'); t_JmaxT_beta.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["JmaxT"]["beta"]
    
    t_RdT_P25 = tt.scalar('t_RdT_P25',  dtype = 'float64'); t_RdT_P25.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["RdT"]["P25"]
    t_RdT_Topt = tt.scalar('t_RdT_Topt',  dtype = 'float64'); t_RdT_Topt.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["RdT"]["Topt"]
    t_RdT_beta = tt.scalar('t_RdT_beta',  dtype = 'float64'); t_RdT_beta.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["RdT"]["beta"]
    
    t_VcmaxT_P25 = tt.scalar('t_VcmaxT_P25',  dtype = 'float64'); t_VcmaxT_P25.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["VcmaxT"]["P25"]
    t_VcmaxT_Topt = tt.scalar('t_VcmaxT_Topt',  dtype = 'float64'); t_VcmaxT_Topt.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["VcmaxT"]["Topt"]
    t_VcmaxT_beta = tt.scalar('t_VcmaxT_beta',  dtype = 'float64'); t_VcmaxT_beta.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["VcmaxT"]["beta"]
    
    t_VpmaxT_P25 = tt.scalar('t_VpmaxT_P25',  dtype = 'float64'); t_VpmaxT_P25.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["VpmaxT"]["P25"]
    t_VpmaxT_Topt = tt.scalar('t_VpmaxT_Topt',  dtype = 'float64'); t_VpmaxT_Topt.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["VpmaxT"]["Topt"]
    t_VpmaxT_beta = tt.scalar('t_VpmaxT_beta',  dtype = 'float64'); t_VpmaxT_beta.tag.test_value = DI_REF_INPUTS["s1_L12_Red00_15C_check"]["VpmaxT"]["beta"]
    
    pdf_ref = pd.read_csv("./reference/s1_L12_Red00_15C_check.csv", header=0)
    t_par = tt.vector('t_par', dtype = 'float64'); t_par.tag.test_value = pdf_ref["par"].values
    t_Ci = tt.vector('t_Ci', dtype = 'float64'); t_Ci.tag.test_value = pdf_ref["ci"].values
    t_tempC = tt.vector('t_tempC',  dtype = 'float64'); t_tempC.tag.test_value = pdf_ref["tempC"].values
    
    t_theta = fTemp(t_theta_P25, t_theta_Topt, t_theta_beta, t_tempC)
    t_alpha_PSII = fTemp(t_alpha_PSII_P25, t_alpha_PSII_Topt, t_alpha_PSII_beta, t_tempC)
    t_JmaxT = fTemp(t_JmaxT_P25, t_JmaxT_Topt, t_JmaxT_beta, t_tempC)
    t_RdT = fTemp(t_RdT_P25, t_RdT_Topt, t_RdT_beta, t_tempC)
    t_VcmaxT = fTemp(t_VcmaxT_P25, t_VcmaxT_Topt, t_VcmaxT_beta, t_tempC)
    t_VpmaxT = fTemp(t_VpmaxT_P25, t_VpmaxT_Topt, t_VpmaxT_beta, t_tempC)
    
    t_Aj = fAj((t_par, t_Ci), t_theta, t_alpha_PSII, t_JmaxT, t_RdT, t_tempC)
    t_Cm = fCm(t_Ci, t_VcmaxT, t_VpmaxT, 10, t_RdT, t_tempC)
    t_Ac1 = fAc1((t_Ci,t_Cm), t_VcmaxT, t_VpmaxT, t_RdT, t_tempC)
    t_Anet = fA(t_Aj, t_Ac1)
      
    theta_model = theano.function(inputs=[t_theta_P25, t_theta_Topt, t_theta_beta, t_tempC],outputs=t_theta)
    alpha_PSII_model = theano.function(inputs=[t_alpha_PSII_P25, t_alpha_PSII_Topt, t_alpha_PSII_beta, t_tempC],outputs=t_alpha_PSII)
    JmaxT_model = theano.function(inputs=[t_JmaxT_P25, t_JmaxT_Topt, t_JmaxT_beta, t_tempC],outputs=t_JmaxT)
    RdT_model = theano.function(inputs=[t_RdT_P25, t_RdT_Topt, t_RdT_beta, t_tempC],outputs=t_RdT)
    VcmaxT_model = theano.function(inputs=[t_VcmaxT_P25, t_VcmaxT_Topt, t_VcmaxT_beta, t_tempC],outputs=t_VcmaxT)
    VpmaxT_model = theano.function(inputs=[t_VpmaxT_P25, t_VpmaxT_Topt, t_VpmaxT_beta, t_tempC],outputs=t_VpmaxT)
    
    Cm_calc = theano.function(inputs=[t_RdT, t_VcmaxT, t_VpmaxT, t_tempC,
                                      t_Ci], 
                               outputs=t_Cm)
    
    Aj_model = theano.function(inputs=[t_theta, t_alpha_PSII, t_JmaxT, t_RdT, t_tempC,
                                       t_par, t_Ci], 
                               outputs=t_Aj)
    
    Ac1_model = theano.function(inputs=[t_RdT, t_VcmaxT, t_VpmaxT, t_tempC,
                                       t_Ci, t_Cm], 
                               outputs=t_Ac1)
    
    PS_model = theano.function(inputs=[t_Aj, t_Ac1], 
                               outputs=t_Anet)

    di_model = {'theta': theta_model,
                'alpha_PSII': alpha_PSII_model,
                'JmaxT': JmaxT_model,
                'RdT': RdT_model,
                'VcmaxT': VcmaxT_model,
                'VpmaxT': VpmaxT_model,
                'Cm': Cm_calc,
                'Aj': Aj_model,
                'Ac1': Ac1_model,
                'PS': PS_model}
    return di_model

def exerciseModel(data,theta_P25,theta_Topt,theta_beta,
                       alpha_PSII_P25,alpha_PSII_Topt,alpha_PSII_beta,
                       JmaxT_P25,JmaxT_Topt,JmaxT_beta,
                       RdT_P25,RdT_Topt,RdT_beta,
                       VcmaxT_P25,VcmaxT_Topt,VcmaxT_beta,
                       VpmaxT_P25,VpmaxT_Topt,VpmaxT_beta,
                       di_model):
    
    (par, Ci, tempC) = data
    
    theta_model = di_model['theta']
    alpha_PSII_model = di_model['alpha_PSII']
    JmaxT_model = di_model['JmaxT']
    RdT_model =di_model['RdT']
    VcmaxT_model = di_model['VcmaxT']
    VpmaxT_model = di_model['VpmaxT']    
    Cm_calc = di_model['Cm']    
    Aj_model = di_model['Aj']
    Ac1_model = di_model['Ac1']
    PS_model = di_model['PS']
    
    simulated_theta = theta_model(theta_P25,theta_Topt,theta_beta,tempC) 
    simulated_alpha_PSII = alpha_PSII_model(alpha_PSII_P25,alpha_PSII_Topt,alpha_PSII_beta,tempC)  
    simulated_JmaxT = JmaxT_model(JmaxT_P25,JmaxT_Topt,JmaxT_beta,tempC)  
    simulated_RdT = RdT_model(RdT_P25,RdT_Topt,RdT_beta,tempC) 
    simulated_VcmaxT = VcmaxT_model(VcmaxT_P25,VcmaxT_Topt,VcmaxT_beta,tempC) 
    simulated_VpmaxT = VpmaxT_model(VpmaxT_P25,VpmaxT_Topt,VpmaxT_beta,tempC)  
    
    simulated_Cm = Cm_calc(simulated_RdT, simulated_VcmaxT, simulated_VpmaxT, tempC, 
                           Ci)
    
    simulated_Aj = Aj_model(simulated_theta, simulated_alpha_PSII, simulated_JmaxT, simulated_RdT, tempC,
                            par, Ci)
    
    simulated_Ac1 = Ac1_model(simulated_RdT, simulated_VcmaxT, simulated_VpmaxT, tempC,
                              Ci, simulated_Cm)    
    
    simulated_Anet = PS_model(simulated_Aj, simulated_Ac1)
    
    di_return = {"Anet": simulated_Anet,
                 "Aj": simulated_Aj,
                 "Ac1": simulated_Ac1,
                 "theta": simulated_theta,
                 "alpha_PSII": simulated_alpha_PSII,
                 "JmaxT": simulated_JmaxT,
                 "RdT": simulated_RdT,
                 "VcmaxT": simulated_VcmaxT,
                 "VpmaxT": simulated_VpmaxT}
    
    return di_return

def processRequest(tu_request,tag):
    pdf_data = tu_request.copy()
    # Generate mock observations to force evaluation of the model at these inputs
    # These could also be generated by np.arange for Qin x Ci parameter combinations
    temps_evaled = ["15C", "20C", "25C", "30C", "35C", "40C"]
    pdf_mock = pd.DataFrame({"curve_type": ["ACi"]* len(temps_evaled),
                            "ID_OBS": ["Mock"]* len(temps_evaled),
                            "A": [np.nan] * len(temps_evaled),
                            "Qin": [1800] * len(temps_evaled),
                            "Ci": [1000] * len(temps_evaled),
                            "temp": temps_evaled})
    pdf_data = pd.concat([pdf_data, pdf_mock])
    pdf_data = pdf_data.sort_values(by = ["curve_type", "ID_OBS"]).reset_index()
    obsAnet = pdf_data["A"].values
    par = pdf_data["Qin"].values
    Ci = pdf_data["Ci"].values
    pdf_data["tempC"] = [float(temp.split("C")[0]) for temp in pdf_data["temp"]]
    tempC = pdf_data["tempC"].values
    
    li_idx_Ci = list(pdf_data[pdf_data["curve_type"] == "ACi"].index)
    li_idx_Ai = list(pdf_data[pdf_data["curve_type"] == "AI0"].index) 

    filename = pdf_data["species"][0] + tag
    
    model_1 = pm.Model()
    with model_1:
        
        alpha_PSII_P25 = pm.Uniform('alpha_PSII_P25', lower=0.01, upper=0.5)
        alpha_PSII_Topt = pm.Uniform('alpha_PSII_Topt', lower=25, upper=49)
        alpha_PSII_beta = pm.Uniform('alpha_PSII_beta', lower=0.01, upper=1.5)
        JmaxT_P25 = pm.Uniform('JmaxT_P25', lower=0.01, upper=600)
        JmaxT_Topt = pm.Uniform('JmaxT_Topt', lower=25, upper=49)
        JmaxT_beta = pm.Uniform('JmaxT_beta', lower=0.01, upper=1.5)
        RdT_P25 = pm.Uniform('RdT_P25', lower=0.01, upper=6)
        RdT_Topt = pm.Uniform('RdT_Topt', lower=25, upper=49)
        RdT_beta = pm.Uniform('RdT_beta', lower=0.01, upper=1.5)
        

        theta = fTemp(0.3,30.0,0.0,tempC)
        alpha_PSII = fTemp(alpha_PSII_P25,alpha_PSII_Topt,alpha_PSII_beta,tempC)
        JmaxT= fTemp(JmaxT_P25,JmaxT_Topt,JmaxT_beta,tempC)
        RDT = fTemp(RdT_P25,RdT_Topt,RdT_beta,tempC)
        Aj = fAj((par, Ci), theta, alpha_PSII, JmaxT, RDT, tempC)
        
        VcmaxT_P25 = pm.Uniform('VcmaxT_P25', lower=0.01, upper=120)
        VcmaxT_Topt = pm.Uniform('VcmaxT_Topt', lower=25, upper=49)
        VcmaxT_beta = pm.Uniform('VcmaxT_beta', lower=0.01, upper=1.5)
        VpmaxT_P25 = pm.Uniform('VpmaxT_P25', lower=0.01, upper=300)
        VpmaxT_Topt = pm.Uniform('VpmaxT_Topt', lower=25, upper=49)
        VpmaxT_beta = pm.Uniform('VpmaxT_beta', lower=0.01, upper=1.5)
        
        VcmaxT = fTemp(VcmaxT_P25,VcmaxT_Topt,VcmaxT_beta,tempC)
        VpmaxT = fTemp(VpmaxT_P25,VpmaxT_Topt,VpmaxT_beta,tempC)
        
        Cm = fCm(Ci,VcmaxT,VpmaxT,10,RDT,tempC)
        Ac1 = fAc1((Ci, Cm), VcmaxT, VpmaxT, RDT, tempC)
        
        sigma_Anet = pm.Uniform('sigma_Anet', lower=1e-6, upper=10)
        Anet = pm.Normal('Anet',
                         mu=fA(Aj,Ac1),
                         sigma=sigma_Anet,
                         observed=obsAnet)
    sys.stdout.flush(); sys.stderr.flush()
    print("sampling...")
    sys.stdout.flush(); sys.stderr.flush()
    print("reading in posterior "+filename+".pickle")
    with open("./posterior_trace/"+filename+".pickle", 'rb') as handle:
        posterior = pickle.load(handle)
    sys.stdout.flush(); sys.stderr.flush()
    def hpd(x,prob):
        hpd_result = az.hdi(x,hdi_prob=prob)
        li=[]
        for key in hpd_result.data_vars.keys():
            li.append([key,hpd_result[key].values[0],hpd_result[key].values[1]])
        pdf_hpd = pd.DataFrame(li, columns=["parameter","hpd_"+str(((1.0-prob)/2)*100),"hpd_"+str((1.0-(1.0-prob)/2)*100)])
        return pdf_hpd
    sys.stdout.flush(); sys.stderr.flush()
    print("generating summary statistics")
    for prob in [0.90,0.94,0.95,0.99]:
        pdf_hpd = hpd(posterior,prob)
        pdf_hpd.to_csv(filename+"_"+str(int(prob*100))+".csv")
    sys.stdout.flush(); sys.stderr.flush()
    
    pdf_summary = pm.summary(posterior)
    
    pdf_posteriorMed = pd.DataFrame({"alpha_PSII_P25": [posterior.posterior.alpha_PSII_P25.median().values],
                                     "alpha_PSII_Topt": [posterior.posterior.alpha_PSII_Topt.median().values],
                                     "alpha_PSII_beta": [posterior.posterior.alpha_PSII_beta.median().values],
                                     "JmaxT_P25": [posterior.posterior.JmaxT_P25.median().values],
                                     "JmaxT_Topt": [posterior.posterior.JmaxT_Topt.median().values],
                                     "JmaxT_beta": [posterior.posterior.JmaxT_beta.median().values],
                                     "RdT_P25": [posterior.posterior.RdT_P25.median().values],
                                     "RdT_Topt": [posterior.posterior.RdT_Topt.median().values],
                                     "RdT_beta": [posterior.posterior.RdT_beta.median().values],
                                     "VcmaxT_P25": [posterior.posterior.VcmaxT_P25.median().values],
                                     "VcmaxT_Topt": [posterior.posterior.VcmaxT_Topt.median().values],
                                     "VcmaxT_beta": [posterior.posterior.VcmaxT_beta.median().values],
                                     "VpmaxT_P25": [posterior.posterior.VpmaxT_P25.median().values],
                                     "VpmaxT_Topt": [posterior.posterior.VpmaxT_Topt.median().values],
                                     "VpmaxT_beta": [posterior.posterior.VpmaxT_beta.median().values],
                                     "sigma_Anet": [posterior.posterior.sigma_Anet.median().values],
                                     }).T.rename(columns = {0: "median"})
    
    pdf_summary = pdf_posteriorMed.merge(pdf_summary, left_index = True, right_index=True)
    pdf_summary = pdf_summary.reset_index()
    pdf_summary = pdf_summary.rename(columns={"index":"parameter"})
    pdf_summary.insert(0,"species", len(pdf_summary)*[filename])
    sys.stdout.flush(); sys.stderr.flush()
    print("writing out summary " + filename)
    pdf_summary.to_csv("./results/"+filename+"_summary.csv")
    sys.stdout.flush(); sys.stderr.flush()
    print("plotting posterior")
    plt.figure()
    pm.traceplot(posterior) 
    plt.savefig("./results/"+filename+"_posterior.png")
    plt.close()
    sys.stdout.flush(); sys.stderr.flush()
    print("building model")
    di_model = buildModel()
    sys.stdout.flush(); sys.stderr.flush()
    print("exercising model")
    
    li_modelSamples = []
    for chain in posterior.sample_stats.chain.values:
        for i in posterior.sample_stats.draw.values:
            di_modelOutput = exerciseModel((par, Ci, tempC),0.3,30,0,
                                                            posterior.posterior.alpha_PSII_P25[chain][i].values,
                                                            posterior.posterior.alpha_PSII_Topt[chain][i].values,
                                                            posterior.posterior.alpha_PSII_beta[chain][i].values,
                                                            posterior.posterior.JmaxT_P25[chain][i].values,
                                                            posterior.posterior.JmaxT_Topt[chain][i].values,
                                                            posterior.posterior.JmaxT_beta[chain][i].values,
                                                            posterior.posterior.RdT_P25[chain][i].values,
                                                            posterior.posterior.RdT_Topt[chain][i].values,
                                                            posterior.posterior.RdT_beta[chain][i].values,
                                                            posterior.posterior.VcmaxT_P25[chain][i].values,
                                                            posterior.posterior.VcmaxT_Topt[chain][i].values,
                                                            posterior.posterior.VcmaxT_beta[chain][i].values,
                                                            posterior.posterior.VpmaxT_P25[chain][i].values,
                                                            posterior.posterior.VpmaxT_Topt[chain][i].values,
                                                            posterior.posterior.VpmaxT_beta[chain][i].values,
                                                            di_model)
            # Add the estimated observation noise to the observed data
            di_modelOutput["Anet"] = di_modelOutput["Anet"] + np.random.normal(0, posterior.posterior.sigma_Anet[chain][i].values)
            li_modelSamples.append(di_modelOutput)
    sys.stdout.flush(); sys.stderr.flush()
    print("writing to dataframe")    
    pdf_modelSamples = pd.DataFrame(li_modelSamples)
    sys.stdout.flush(); sys.stderr.flush()    
    
    uniqueTemps = [15,20,25,30,35,40]
    di_tempResponseParams = {}
    for temperature in uniqueTemps:
        sys.stdout.flush(); sys.stderr.flush()
        print("processing temperature "+ str(temperature))
        # First, grab the temperature response parameters for later plotting
        pdf_subData_temp = pdf_data[(pdf_data["tempC"] == temperature)]
                
        # For a given temperature, all values will be the same, so just use the mean to compress that axis.
        alpha_PSII_samples = np.stack(pdf_modelSamples["alpha_PSII"].values)[:, pdf_subData_temp.index].mean(axis = 1)
        JmaxT_samples = np.stack(pdf_modelSamples["JmaxT"].values)[:, pdf_subData_temp.index].mean(axis = 1)
        RdT_samples = np.stack(pdf_modelSamples["RdT"].values)[:, pdf_subData_temp.index].mean(axis = 1)
        VcmaxT_samples = np.stack(pdf_modelSamples["VcmaxT"].values)[:, pdf_subData_temp.index].mean(axis = 1)
        VpmaxT_samples = np.stack(pdf_modelSamples["VpmaxT"].values)[:, pdf_subData_temp.index].mean(axis = 1)
        
        di_tempResponseParams[temperature] = {"alpha_PSII_samples": alpha_PSII_samples,
                                              "JmaxT_samples": JmaxT_samples,
                                              "RdT_samples": RdT_samples,
                                              "VcmaxT_samples": VcmaxT_samples,
                                              "VpmaxT_samples": VpmaxT_samples}

        # Next, generate the Ci plot for this temperature.
        pdf_subData_tempByCi = pdf_data[(pdf_data["tempC"] == temperature) &
                                       (pdf_data["curve_type"] == "ACi")]
        pdf_subData_tempByCi = pdf_subData_tempByCi.sort_values("Ci")
        Anet_samples = np.stack(pdf_modelSamples["Anet"].values)[:, pdf_subData_tempByCi.index]
        Aj_samples = np.stack(pdf_modelSamples["Aj"].values)[:, pdf_subData_tempByCi.index]
        Ac1_samples = np.stack(pdf_modelSamples["Ac1"].values)[:, pdf_subData_tempByCi.index]
        
        # Ci plot
        figCi = plt.figure(figsize=(5,5))
        row = 10
        col = 10
        ax_ci = plt.subplot2grid((row, col), (0, 0), rowspan = 10, colspan=10)
        
        pdf_Ci_plot = pd.DataFrame({"Ci": pdf_subData_tempByCi["Ci"],
                                    "Anet_sampled": Anet_samples.mean(axis=0),
                                    "Aj_sampled": Aj_samples.mean(axis=0),
                                    "Ac1_sampled": Ac1_samples.mean(axis=0) ,
                                    "Anet_observed": pdf_subData_tempByCi["A"],
                                    "idx": pdf_subData_tempByCi.index})

        # Due to precence of mock obsevations that should have np.nan "observed" data, drop for plotting actual observations.
        ax_ci.plot(pdf_Ci_plot.dropna()["Ci"], pdf_Ci_plot.dropna()["Anet_observed"], "o", ms=4, alpha=0.4, label="Anet Data", color='black', fillstyle='none')
        ax_ci.plot(pdf_Ci_plot["Ci"], pdf_Ci_plot["Anet_sampled"], label="Mean outcome", alpha=0.6, color = "orange")
        ax_ci.plot(pdf_Ci_plot["Ci"], pdf_Ci_plot["Ac1_sampled"], label="Ac1 mean outcome", alpha=0.6, color = "cornflowerblue")
        ax_ci.plot(pdf_Ci_plot["Ci"], pdf_Ci_plot["Aj_sampled"], label="Aj mean outcome", alpha=0.6, color = "indianred")
        
        j=0
        transition = len(pdf_Ci_plot)
        for i,row in pdf_Ci_plot.iterrows():
            if pdf_Ci_plot["Ac1_sampled"][i] >= pdf_Ci_plot["Aj_sampled"][i]:
                transition = j
                break;
            j+=1
            
                
        ax_ci.plot(pdf_Ci_plot["Ci"][:transition+1], pdf_Ci_plot["Ac1_sampled"][:transition+1], alpha=0.9, color = "cornflowerblue", linewidth=2.5)
        if j !=0:
            ax_ci.plot(pdf_Ci_plot["Ci"][transition:], pdf_Ci_plot["Aj_sampled"][transition:], alpha=0.9, color = "indianred", linewidth=2.5)
        
        az.plot_hdi(
            pdf_Ci_plot["Ci"],
            Anet_samples,
            hdi_prob=0.94,
            smooth=False,
            ax=ax_ci,
            fill_kwargs={"alpha": 0.4, "color": "orange", "label": "Outcome 94% HPD"})   
           
        ax_ci.set_xlabel("C"r'$_{i}$'" ("r'$\mu$'"bar)")
        ax_ci.set_ylabel("Leaf A"r'$_{n}$'" ("r'$\mu$'"mol/m"r'$_{2}$'" leaf/s)")
        ax_ci.set_xlim([0,1000])
        ax_ci.set_ylim([0,60])
        ax_ci.xaxis.set_minor_locator(MultipleLocator(100))
        figCi.savefig("./results/"+filename+"_ppc_ci_"+str(temperature)+".png", dpi = 600)
        plt.close()


        # Next, generate the Par plot for this temperature.
        pdf_subData_tempByPar = pdf_data[(pdf_data["tempC"] == temperature) &
                                       (pdf_data["curve_type"] == "AI0")]
        pdf_subData_tempByPar = pdf_subData_tempByPar.sort_values("Qin")
        
        Anet_samples = np.stack(pdf_modelSamples["Anet"].values)[:, pdf_subData_tempByPar.index]
        Aj_samples = np.stack(pdf_modelSamples["Aj"].values)[:, pdf_subData_tempByPar.index]
        Ac1_samples = np.stack(pdf_modelSamples["Ac1"].values)[:, pdf_subData_tempByPar.index]
        
        # par plot
        figpar = plt.figure(figsize=(5,5))
        row = 10
        col = 10
        ax_par = plt.subplot2grid((row, col), (0, 0), rowspan = 10, colspan=10)
        
        pdf_par_plot = pd.DataFrame({"par": pdf_subData_tempByPar["Qin"],
                                    "Anet_sampled": Anet_samples.mean(axis=0),
                                    "Aj_sampled": Aj_samples.mean(axis=0),
                                    "Ac1_sampled": Ac1_samples.mean(axis=0) ,
                                    "Anet_observed": pdf_subData_tempByPar["A"],
                                    "idx": pdf_subData_tempByPar.index})
        ax_par.plot(pdf_par_plot["par"], pdf_par_plot["Anet_observed"], "o", ms=4, alpha=0.4, label="Anet Data", color='black', fillstyle='none')
        ax_par.plot(pdf_par_plot["par"], pdf_par_plot["Anet_sampled"], label="Mean outcome", alpha=0.6, color = "orange")
        ax_par.plot(pdf_par_plot["par"], pdf_par_plot["Ac1_sampled"], label="Ac1 mean outcome", alpha=0.6, color = "cornflowerblue")
        ax_par.plot(pdf_par_plot["par"], pdf_par_plot["Aj_sampled"], label="Aj mean outcome", alpha=0.6, color = "indianred")
        j=0
        transition = len(pdf_par_plot)
        for i, row in pdf_par_plot.iterrows():
            if pdf_par_plot["Ac1_sampled"][i] <= pdf_par_plot["Aj_sampled"][i]:
                transition = j
                break;
            j+=1
                        
        if j !=0:        
            ax_par.plot(pdf_par_plot["par"][transition:], pdf_par_plot["Ac1_sampled"][transition:], alpha=0.9, color = "cornflowerblue", linewidth=2.5)
        ax_par.plot(pdf_par_plot["par"][:transition+1], pdf_par_plot["Aj_sampled"][:transition+1], alpha=0.9, color = "indianred", linewidth=2.5)
        
        az.plot_hdi(
            pdf_par_plot["par"],
            Anet_samples,
            hdi_prob=0.94,
            smooth=False,
            ax=ax_par,
            fill_kwargs={"alpha": 0.4, "color": "orange", "label": "Outcome 94% HPD"})   
           
        ax_par.set_xlabel("PPFD ("r'$\mu$'"mol/m"r'$_{2}$'" leaf/s)")
        ax_par.set_ylabel("Leaf A"r'$_{n}$'" ("r'$\mu$'"mol/m"r'$_{2}$'" leaf/s)")
        ax_par.set_xlim([0,2750])
        ax_par.set_ylim([0,60])
        ax_par.xaxis.set_minor_locator(MultipleLocator(100))
        figpar.savefig("./results/"+filename+"_ppc_par_"+str(temperature)+".png", dpi = 600)
        plt.close()
        sys.stdout.flush(); sys.stderr.flush()
        
    pdf_tempResponseParams = pd.DataFrame(di_tempResponseParams).T.reset_index().rename(columns={"index":"temperature"})
    pdf_tempResponseParams = pdf_tempResponseParams.sort_values("temperature")
    paramList = ["alpha_PSII", "JmaxT", "RdT", "VcmaxT", "VpmaxT"]
                 
    for paramName in paramList:
        
        # Now plot the temperature responses
        figTempResponse = plt.figure(figsize=(5,5))
        row = 10
        col = 10
        ax_temp = plt.subplot2grid((row, col), (0, 0), rowspan = 10, colspan=10)
        
        ax_temp.plot(pdf_tempResponseParams["temperature"], pdf_tempResponseParams[paramName + "_samples"].apply(np.mean), label="Mean outcome", alpha=0.6, color = "orange")
    
        az.plot_hdi(
            pdf_tempResponseParams["temperature"].values,
            np.stack(pdf_tempResponseParams[paramName + "_samples"].values).T,
            hdi_prob=0.94,
            smooth=False,
            ax=ax_temp,
            fill_kwargs={"alpha": 0.4, "color": "orange", "label": "Outcome 94% HPD"})   
           
        ax_temp.set_xlabel("Temperature")
        ax_temp.set_ylabel(paramName)
        ax_temp.set_title("Posterior predictive checks; T=" + str(temperature))
        ax_temp.legend(ncol=2, fontsize=10);
        figTempResponse.savefig("./results/"+filename+"_ppc_" + paramName + ".png", dpi = 600)
        plt.close()    

def test_reference(DI_REF_INPUTS):
    
    for treat in DI_REF_INPUTS.keys():
        
        pdf_ref = pd.read_csv("./reference/"+treat+".csv",header=0)
        par = pdf_ref["par"].values
        Ci = pdf_ref["ci"].values
        tempC = pdf_ref["tempC"].values
        Anet_excel = pdf_ref["A"].values
        Aj_excel = pdf_ref["Aj"].values
        Ac1_excel = pdf_ref["Ac1"].values
        
        theta_beta = DI_REF_INPUTS[treat]["theta"]["beta"]
        theta_P25 = DI_REF_INPUTS[treat]["theta"]["P25"]
        theta_Topt = DI_REF_INPUTS[treat]["theta"]["Topt"]
        alpha_PSII_beta = DI_REF_INPUTS[treat]["alpha_PSII"]["beta"]
        alpha_PSII_P25 = DI_REF_INPUTS[treat]["alpha_PSII"]["P25"]
        alpha_PSII_Topt = DI_REF_INPUTS[treat]["alpha_PSII"]["Topt"]
        JmaxT_beta = DI_REF_INPUTS[treat]["JmaxT"]["beta"]
        JmaxT_P25 = DI_REF_INPUTS[treat]["JmaxT"]["P25"]
        JmaxT_Topt = DI_REF_INPUTS[treat]["JmaxT"]["Topt"]
        RdT_beta = DI_REF_INPUTS[treat]["RdT"]["beta"]
        RdT_P25 = DI_REF_INPUTS[treat]["RdT"]["P25"]
        RdT_Topt = DI_REF_INPUTS[treat]["RdT"]["Topt"]
        VcmaxT_beta = DI_REF_INPUTS[treat]["VcmaxT"]["beta"]
        VcmaxT_P25 = DI_REF_INPUTS[treat]["VcmaxT"]["P25"]
        VcmaxT_Topt = DI_REF_INPUTS[treat]["VcmaxT"]["Topt"]
        VpmaxT_beta = DI_REF_INPUTS[treat]["VpmaxT"]["beta"]
        VpmaxT_P25 = DI_REF_INPUTS[treat]["VpmaxT"]["P25"]
        VpmaxT_Topt = DI_REF_INPUTS[treat]["VpmaxT"]["Topt"]
        
            
        di_return = exerciseModel((par,Ci,tempC),theta_P25,theta_Topt,theta_beta,
                                                 alpha_PSII_P25,alpha_PSII_Topt,alpha_PSII_beta,
                                                 JmaxT_P25,JmaxT_Topt,JmaxT_beta,
                                                 RdT_P25,RdT_Topt,RdT_beta,
                                                 VcmaxT_P25,VcmaxT_Topt,VcmaxT_beta,
                                                 VpmaxT_P25,VpmaxT_Topt,VpmaxT_beta)
        Anet = di_return["Anet"]
        Aj = di_return["Aj"]
        Ac1 = di_return["Ac1"]
        sumError_Anet = (Anet_excel - Anet).sum()
        sumError_Aj = (Aj_excel - Aj).sum()
        sumError_Ac1 = (Ac1_excel - Ac1).sum()
        print("\nErrors against reference", treat, ":", sumError_Anet, sumError_Aj, sumError_Ac1)




if __name__ == "__main__":
    di_fp_posteriors = {#('sorghum', "all"):'all/sorghum_all.pickle',
                        ('sorghum', "drop"):'drop/sorghum.pickle',
                        #('maize', "all"):'all/maize_all.pickle',
                        ('maize', "drop"):'drop/maize.pickle'}
    
    for key in di_fp_posteriors.keys():
        tag = "_"+key[1]
        sys.stdout.flush(); sys.stderr.flush()
        print(str(key) + "start time = ", str(time.time()))
        sys.stdout.flush(); sys.stderr.flush()
        pdf = pd.read_csv("./ACi_AI_data.csv", header=0)
        pdf = pdf.dropna()  
        pdf.loc[pdf[pdf["treat"]=="s2_L10_ACi_Blue0_30C_2020Nov30_light_ON"].index, ["rep","treat"]] = "s2-1","s2-1_L10_Aci_Blue0_30C_2020Nov30_light_ON"
        pdf.loc[pdf[pdf["treat"]=="s2_L10_AI0_Blue0_30C_2020Nov30_clear_ON"].index, ["rep","treat"]] = "s2-1","s2-1_L10_AI0_Blue0_30C_2020Nov30_clear_ON"
        pdf.loc[pdf[pdf["treat"]=="s2_L10_ACi_Blue0_30C_2020Nov30_trans_ON"].index, ["rep","treat"]] = "s2-2","s2-2_L10_ACi_Blue0_30C_2020Nov30_trans_ON"
        pdf.loc[pdf[pdf["treat"]=="s2_L10_AI0_Blue0_30C_2020Nov30_00000_ON"].index, ["rep","treat"]] = "s2-2","s2-2_L10_AI0_Blue0_30C_2020Nov30_00000_ON"
        
        if key[1] == "drop":
            li_drop_treats = ["m1_L13_ACi_Red00_15C_2020Dec07",
                  "m2_L11_ACi_Green_20C_2020Dec03",
                  "m1_L09_ACi_Blue0_25C_2020Dec01",
                  "m4_L10_ACi_Green_35C_2020Dec02",
                  "s1_L12_ACi_Red00_15C_2020Dec07",
                  "s2_L11_ACi_Blue0_15C_2020Dec11",
                  "s3_L11_ACi_Blue0_15C_2020Dec07",
                  "s1_L11_ACi_Blue0_20C_2020Dec03",
                  "s1_L12_ACi_Blue0_20C_2020Dec10",
                  "s2_L11_ACi_Red00_20C_2020Dec03",
                  "s3_L11_ACi_Green_20C_2020Dec03",
                  "s1_L11_ACi_Green_25C_2020Dec01",
                  "s1_L11_ACi_Red00_25C_2020Dec01",
                  "s2_L11_ACi_Blue0_25C_2020Dec09",
                  "s3_L11_ACi_Blue0_25C_2020Dec01",
                  "s3_L11_ACi_Blue0_25C_2020Dec09",
                  "m1_L13_AI0_Red00_15C_2020Dec07",
                  "m2_L11_AI0_Green_20C_2020Dec03",
                  "m1_L09_AI0_Blue0_25C_2020Dec01",
                  "m4_L10_AI0_Green_35C_2020Dec02",
                  "s1_L12_AI0_Red00_15C_2020Dec07",
                  "s2_L11_AI0_Blue0_15C_2020Dec11",
                  "s3_L11_AI0_Blue0_15C_2020Dec07",
                  "s1_L11_AI0_Blue0_20C_2020Dec03",
                  "s1_L12_AI0_Blue0_20C_2020Dec10",
                  "s2_L11_AI0_Red00_20C_2020Dec03",
                  "s3_L11_AI0_Green_20C_2020Dec03",
                  "s1_L11_AI0_Green_25C_2020Dec01",
                  "s1_L11_AI0_Red00_25C_2020Dec01",
                  "s2_L11_AI0_Blue0_25C_2020Dec09",
                  "s3_L11_AI0_Blue0_25C_2020Dec01",
                  "s3_L11_AI0_Blue0_25C_2020Dec09"]
            li_drop = []
            pdf_drop = pd.DataFrame(li_drop_treats)
            for i, rowi in pdf.iterrows():
                for treat in li_drop_treats:
                    if treat in rowi["treat"]:
                        li_drop.append(rowi["treat"])
            pdf = pdf[~pdf["treat"].isin(li_drop)]
        
        di_species = {"s":"sorghum","m":"maize"}
        pdf["species"]=[di_species[treat.split("_")[0][0]] for treat in pdf["treat"]]

        li_tu_EUrequests = [pdf[pdf["species"]==sample] for sample in [key[0]]]
        sys.stdout.flush(); sys.stderr.flush()
        print("observations = " + str(len(li_tu_EUrequests[0])))
        sys.stdout.flush(); sys.stderr.flush()
        processRequest(li_tu_EUrequests[0], tag)
        sys.stdout.flush(); sys.stderr.flush()
        print(str(key) + "end time = ", str(time.time()))
        sys.stdout.flush(); sys.stderr.flush()
