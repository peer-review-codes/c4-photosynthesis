# c4 photosynthesis probabilistic model
This is a python probabilistic programming implementation of C4 photosynthesis the basis is as described by Susanne von Caemmerer (2021; https://doi.org/10.1093/jxb/erab266) with extensions made by Wu et al. (2023; in review; doi TBD).

This is hosted here by Corteva Agriscience as a component of their contribution to the research pursued and collaborated between Predictive Agriculture at CTVA and Professor Graeme Hammer's research group at the University of Queensland, Australia (2019-2021).
